"""All database requisites"""

# -*- coding: utf-8 -*-
DATABASE = 'bot_control'
HOST = 'localhost'
USERNAME = 'login_database'
PASSWORD = 'password_database'

TABLE_USERS = 'users_bots'
TABLE_BOTS = 'control_bots'
TABLE_NOTIFY = 'notify_bots'

TABLE_USERS_ROWS = "("\
    "user_id int unsigned NOT NULL DEFAULT 0, "\
    "username varchar(200) NOT NULL DEFAULT '', "\
    "is_admin tinyint NOT NULL DEFAULT 0"\
    ")"

TABLE_BOTS_ROWS = "("\
    "botname varchar(200) NOT NULL UNIQUE DEFAULT '', "\
    "username_bot varchar(200) NOT NULL DEFAULT '', "\
    "timelaunch datetime NOT NULL DEFAULT 0, "\
    "launchpath varchar(2048) NOT NULL DEFAULT '', "\
    "dirpath varchar(2048) NOT NULL DEFAULT '', "\
    "status tinyint NOT NULL DEFAULT 0, "\
    "process_pid int unsigned NOT NULL DEFAULT 0"\
    ")"

TABLE_NOTIFY_ROWS = "("\
    "user_id int unsigned NOT NULL DEFAULT 0, "\
    "username varchar(200) NOT NULL DEFAULT '', "\
    "botname varchar(200) NOT NULL DEFAULT ''"\
    ")"
