"""Classes and methods for work with bot controll telegram bot databases"""

# -*- coding: utf-8 -*-

import pymysql
import lib.db_data as db_data


class BotControlDatabase():
    """Work with clients table"""
    def __init__(self, table, table_data):
        self.host = db_data.HOST
        self.username = db_data.USERNAME
        self.password = db_data.PASSWORD
        self.database = db_data.DATABASE
        self.table = table
        self.table_data = table_data

        self.create_table()

    def _connect(self):
        """Connect to database by self data"""
        connect = pymysql.connect(
            host=self.host,
            user=self.username,
            password=self.password,
            db=self.database
            )
        return connect

    def _execute_query_without_return(self, query):
        """Execute simple query to database"""
        connect = self._connect()
        try:
            with connect.cursor() as cursor:
                cursor.execute(query)
            connect.commit()
        finally:
            connect.close()

    def _execute_query_with_return(self, query):
        """Execute simple query and returns data"""
        connect = self._connect()

        try:
            with connect.cursor() as cursor:
                cursor.execute(query)
                database_data = cursor.fetchall()
            connect.commit()
        finally:
            connect.close()
        return database_data

    def create_table(self):
        """Creates table in start if it isn't exist"""
        query = "CREATE TABLE IF NOT EXISTS %s %s "\
            "CHARACTER SET utf8 COLLATE utf8_general_ci" %\
            (self.table, self.table_data)

        self._execute_query_without_return(query)


class BotWork(BotControlDatabase):
    """Work with bot table"""
    def __init__(self):
        super().__init__(db_data.TABLE_BOTS, db_data.TABLE_BOTS_ROWS)

    def check_for_new_bot(self, botname):
        query = "SELECT * FROM %s WHERE botname='%s'" % (self.table, botname)

        query_data = self._execute_query_with_return(query)
        if len(query_data) == 0:
            return True
        return False

    def add_bot(self, botname, launchpath, dirpath, username):
        """Adds new bot data in database"""
        query = "REPLACE  INTO %s "\
            "(botname, launchpath, dirpath, username_bot) "\
            "VALUES ('%s', '%s', '%s', '%s')" %\
            (self.table, botname, launchpath, dirpath, username)
        self._execute_query_without_return(query)

    def get_all_data_of_bot(self, botname):
        """Returns all bot's data by botname"""
        query = "SELECT * FROM %s WHERE botname='%s'" %\
            (self.table, botname)

        bot_data = self._execute_query_with_return(query)
        return bot_data[0]

    def update_bot_status(self, botname, status, pid=0):
        """Updates bot status byt botname"""
        if status == 1:
            query = "UPDATE %s SET status=%d, timelaunch=NOW(), "\
                "process_pid=%d WHERE botname='%s'"\
                % (self.table, status, pid, botname)
        else:
            query = "UPDATE %s SET status=%d WHERE botname='%s'" %\
                (self.table, status, botname)

        self._execute_query_without_return(query)

    def get_process_bot_pid(self, botname):
        """Returns bot's pid from database"""
        query = "SELECT process_pid FROM %s WHERE botname='%s'" %\
            (self.table, botname)

        process_pid = self._execute_query_with_return(query)

        return process_pid[0]

    def get_bots_time_diffirence(self, username):
        """Returns time diffirence in seconds for bots list"""
        query = "SELECT is_admin FROM %s WHERE username='%s'" %\
            (db_data.TABLE_USERS, username)

        try:
            is_admin = self._execute_query_with_return(query)[0][0]
        except IndexError:
            is_admin = False
        finished_times = []

        if is_admin:
            botnames_query = "SELECT botname FROM %s" % self.table
        else:
            botnames_query = "SELECT botname FROM %s WHERE username='%s'" %\
                (db_data.TABLE_NOTIFY, username)

        botnames = self._execute_query_with_return(botnames_query)

        for botname in botnames:
            seconds_diff_query = "SELECT NOW() - "\
                "(SELECT timelaunch FROM %s WHERE botname='%s' AND status=1)" %\
                (self.table, botname[0])
            get_seconds = self._execute_query_with_return(seconds_diff_query)
            if get_seconds[0][0] is None:
                finished_times.append(0)
            else:
                finished_times.append(get_seconds[0][0])

        return finished_times

    def get_bots_data(self, username):
        """Returns bots name, bots status and path to bots scripts"""
        query = "SELECT is_admin FROM %s WHERE username='%s'" %\
            (db_data.TABLE_USERS, username)

        try:
            is_admin = self._execute_query_with_return(query)[0][0]
        except IndexError:
            is_admin = False
        finished_bots_data = []

        if is_admin:
            botnames_query = "SELECT botname FROM %s" % self.table
        else:
            botnames_query = "SELECT botname FROM %s WHERE username='%s'" %\
                (db_data.TABLE_NOTIFY, username)

        botnames = self._execute_query_with_return(botnames_query)

        for botname in botnames:
            botquery = "SELECT botname, username_bot, status "\
                "FROM %s WHERE botname='%s'" % (self.table, botname[0])
            tmp_bot_data = self._execute_query_with_return(botquery)
            finished_bots_data.append(tmp_bot_data)

        return finished_bots_data

    def get_bots_names(self):
        """Returns all bots names"""
        query = "SELECT botname FROM %s" % self.table

        bots_names = self._execute_query_with_return(query)
        return bots_names

    def get_active_botnames(self):
        query = "SELECT botname FROM %s WHERE status=1" % self.table

        bots_names = self._execute_query_with_return(query)
        return bots_names

    def get_bot_status(self, botname):
        """Returns bots status 1 or 0"""
        query = "SELECT status FROM %s WHERE botname='%s'" %\
            (self.table, botname)

        status = self._execute_query_with_return(query)[0][0]
        return status

    def get_lauchpath_and_botnames(self):
        """Returns all launchpath and botname if bot's status is equal 1"""
        query = "SELECT launchpath, botname FROM %s WHERE status=1" %\
            self.table

        bots_data = self._execute_query_with_return(query)
        return bots_data

    def get_botpath_by_botname(self, botname):
        """Returns path of bot"""
        query = "SELECT dirpath FROM %s WHERE botname='%s'" %\
            (self.table, botname)

        path = self._execute_query_with_return(query)[0][0]
        return path

    def remove_bot_by_botname(self, botname):
        """Removes bot from table by botname"""
        query = "DELETE FROM %s WHERE botname='%s'" % (self.table, botname)

        self._execute_query_without_return(query)

    def get_pid_and_botnames(self):
        """Returns botnames and pids from table"""
        query = "SELECT process_pid, botname FROM %s WHERE status=1" \
            % self.table

        query_data = self._execute_query_with_return(query)
        return query_data


class UserWork(BotControlDatabase):
    """Work with users database for bots control telegram bot"""
    def __init__(self):
        super().__init__(db_data.TABLE_USERS, db_data.TABLE_USERS_ROWS)

    def add_new_user(self, user_id, username):
        """Adds new user in table"""
        query = "INSERT IGNORE INTO %s (user_id, username) VALUES(%d, '%s')" %\
            (self.table, user_id, username)
        self._execute_query_without_return(query)

    def get_userlist(self):
        """Returns all users from table"""
        query = "SELECT username FROM %s" % self.table

        usernames = self._execute_query_with_return(query)
        return usernames

    def get_user_id(self, username):
        """Return user's id by username"""
        query = "SELECT user_id FROM %s WHERE username='%s'" %\
            (self.table, username)

        user_id = self._execute_query_with_return(query)[0][0]
        return user_id

    def get_all_users_id(self):
        """Returns all users id from users table"""
        query = "SELECT user_id FROM %s" % self.table

        users_id = self._execute_query_with_return(query)
        return users_id

    def admin_level(self, username):
        """Checks user for admin rules"""
        query = "SELECT is_admin FROM %s WHERE username='%s'" %\
            (self.table, username)

        status = self._execute_query_with_return(query)[0][0]
        return status

    def get_regular_userlist(self):
        """Returns userlist withour admin access"""
        query = "SELECT username FROM %s WHERE is_admin=0" % self.table

        userlist = self._execute_query_with_return(query)
        return userlist

    def get_admin_userlist(self):
        """Returns userlisth with admin access"""
        query = "SELECT username FROM %s WHERE is_admin=1" % self.table

        adminlist = self._execute_query_with_return(query)
        return adminlist

    def get_superadmins_ids(self):
        """Returns admins 2 level for get user's messagies"""
        query = "SELECT user_id FROM %s WHERE is_admin=2" % self.table

        superadmins_id = self._execute_query_with_return(query)
        return superadmins_id

    def increase_rules_and_return_id(self, username):
        query1 = "UPDATE %s SET is_admin=1 WHERE username='%s'" %\
            (self.table, username)
        query2 = "SELECT user_id FROM %s WHERE username='%s'" %\
            (self.table, username)
        self._execute_query_without_return(query1)
        user_id = self._execute_query_with_return(query2)[0][0]

        return user_id

    def reduce_rules_and_return_id(self, username):
        query1 = "UPDATE %s SET is_admin=0 WHERE username='%s'" %\
            (self.table, username)
        query2 = "SELECT user_id FROM %s WHERE username='%s'" %\
            (self.table, username)
        self._execute_query_without_return(query1)
        user_id = self._execute_query_with_return(query2)[0][0]

        return user_id


class NotifyWork(BotControlDatabase):
    """Work for notify control for bots control telegram bot"""
    def __init__(self):
        super().__init__(db_data.TABLE_NOTIFY, db_data.TABLE_NOTIFY_ROWS)

    def get_userlist_by_botname(self, botname):
        """Returns userlist by botname from notification table"""
        query = "SELECT username FROM %s WHERE botname='%s'" %\
            (self.table, botname)

        userlist = self._execute_query_with_return(query)
        return userlist

    def get_users_id_by_botname(self, botname):
        """Returns all users id for notify users"""
        query = "SELECT user_id FROM %s WHERE botname='%s'" %\
            (self.table, botname)

        users_id = self._execute_query_with_return(query)
        return users_id

    def check_for_double(self, user_id, botname):
        """Checks for double rows in table"""
        query = "SELECT * FROM %s WHERE user_id=%d AND botname='%s'" %\
            (self.table, user_id, botname)

        row = self._execute_query_with_return(query)
        if len(row) == 0:
            return True
        return False

    def update_bot_for_user(self, user_id, username, botname):
        """Adds bot for user in users table"""
        query = "INSERT INTO %s VALUES(%d, '%s', '%s')" %\
            (self.table, user_id, username, botname)

        self._execute_query_without_return(query)

    def delete_bot_for_user(self, username, botname):
        """Deletes data from notify table"""
        query = "DELETE FROM %s WHERE username='%s' AND botname='%s'" %\
            (self.table, username, botname)

        self._execute_query_without_return(query)

    def remove_all_notify_of_botname(self, botname):
        """Removes all notify by botname for delete bot from controller"""
        query = "DELETE FROM %s WHERE botname='%s'" % (self.table, botname)

        self._execute_query_without_return(query)
