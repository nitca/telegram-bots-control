## About
Telegram for for control telegram bots.

Throught the bot you can:
- add your bot on server throught telegram;
- start, stop, restart bots;
- read logs of bot;
- set notification about work of bot for users;

## Before start
Create mysql database **bot_control** and give access for your user

Set your token in config.py

Set your login and password to database in lib/db_data.py

## User groups
In the bot have 3 groups setting up in users_bot table in column is_admin

0 \- user, user can only gets notifications about work of bots

1 \- admin, controls the controller bot

2 \- root, controls the controller bot and sets new admins

The rules gives throught bot except for root. Root gives only throught database
All tables creates after bot lauch

## How to add bot in the controller
Copy example bot_control.ini in your bot
Set your bot data in the file
Archive your bot in zip archive
Send your bot in menu "Добавить бота"
